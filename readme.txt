
==== CHANGELOG ====


Version 1.2*
- Updated to iOS11, Xcode 9 & Swift 4
- Updated AdMob SDK

* This is a BRAND NEW version of the code; almost all files have been changed – To update you will need to start to set up your app with this version.


Version 1.0 
- Initial release





