//
//  WorkoutTimerViewController.swift
//  FitnessStudio
//
//  Created by Greenfield
//  Copyright (c) 2017 Greenfield.com. All rights reserved.
//


import UIKit
import CLTimer
import AudioToolbox


class WorkoutTimerViewController: UIViewController,cltimerDelegate {

    
    
    @IBOutlet weak var startBtn: UIButton!
    @IBOutlet weak var workoutTimer: CLTimer!
    
  
    
    var isTimerStarted = false
    
    let k5_MINUTES = 300
    let k10_MINUTES = 600
    let k15_MINUTES = 900
    let k20_MINUTES = 1200
    let k30_MINUTES = 1800
    var currentSecond = 300

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        workoutTimer.cltimer_delegate = self
        


    }

    

    
    @IBAction func startBtnDidTap(_ sender: UIButton) {
        isTimerStarted = !isTimerStarted
        if isTimerStarted {
        //start timer
            sender.setTitle("STOP", for: UIControlState.normal)
            startTimer(withSecond: currentSecond)
        } else {
        //reset timer
            sender.setTitle("START", for: UIControlState.normal)
            resetTimer()
        }
        
        
    }
    
    
    func resetTimer(){
        workoutTimer.resetTimer()
        
    }
    
    
    func startTimer(withSecond:Int){
    
    workoutTimer.startTimer(withSeconds: currentSecond, format: .Minutes, mode: .Reverse)
    
    }
    
    
    @IBAction func durationSwitchedDidChange(_ sender: UISegmentedControl) {
        workoutTimer.stopTimer()
        workoutTimer.resetTimer()
        isTimerStarted = false
        startBtn.setTitle("START", for: UIControlState.normal)
        
        switch sender.selectedSegmentIndex {
        case 0:
            currentSecond = k5_MINUTES
            workoutTimer.updateText(timeValue: "5 min")
        case 1:
            currentSecond = k10_MINUTES
            workoutTimer.updateText(timeValue: "10 min")

        case 2:
            currentSecond = k15_MINUTES
            workoutTimer.updateText(timeValue: "15 min")

        case 3:
            currentSecond = k20_MINUTES
            workoutTimer.updateText(timeValue: "20 min")

        default:
            currentSecond = k5_MINUTES
            workoutTimer.updateText(timeValue: "5 min")

        }
    }
    
    @IBAction func backToMainView(_ sender: UIBarButtonItem) {
        categoryStr = ""
        self.tabBarController?.navigationController?.popViewController(animated: true)
        self.tabBarController?.navigationController?.isNavigationBarHidden = false
    }
    
    func playFinishSound (){
    if let soundURL = Bundle.main.url(forResource: "bing", withExtension: "aiff") {
    var mySound: SystemSoundID = 0
    AudioServicesCreateSystemSoundID(soundURL as CFURL, &mySound)
    AudioServicesPlaySystemSound(mySound);
        }
    
    }
    
    func timerDidStop(time: Int) {
        
        
    }
    
    
    func timerDidUpdate(time: Int) {
        
       print("time : \(time)")
        if time == 0 {
        playFinishSound()
        isTimerStarted = false
        startBtn.setTitle("START", for: UIControlState.normal)

        
        }
        
    }
    
       
}
