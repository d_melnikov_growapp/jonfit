//
//  MainMenuViewController.swift
//  FitnessStudio
//
//  Created by Денис on 04/12/2018.
//  Copyright © 2018 GF. All rights reserved.
//

import UIKit
import Parse

class MainMenuViewController: UIViewController {
        
    override func viewWillAppear(_ animated: Bool) {
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "logo")
        imageView.image = image
        navigationItem.titleView = imageView
        
        if PFUser.current() != nil {
            categoryStr = "All Exercises"
            if firstStartup {
                firstStartup = false
            }
        } else {
            let aVC = storyboard?.instantiateViewController(withIdentifier: "Login") as! Login
            present(aVC, animated: true, completion: nil)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    //MARK: - Methods
    func useTabBar(selectIndexViewController: Int) {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
        vc.selectedIndex = selectIndexViewController
        self.navigationController?.pushViewController(vc, animated: true)
        vc.navigationController?.isNavigationBarHidden = true
    }
    
    //Update global vars in exercises
    func refreshGlobalVariables(category: String) {
        categoryStr = category
    }
    
    //MARK: - Actions
    @IBAction func workoutsActionButton(_ sender: UIButton) {
        useTabBar(selectIndexViewController: Constants.indexOfWorkoutVC)
    }
    
    @IBAction func healthyRecipesActionButton(_ sender: UIButton) {
        refreshGlobalVariables(category: "Healthy Recipes")
        useTabBar(selectIndexViewController: Constants.indexOfWorkoutVC)
    }
    
    @IBAction func prainingProgramsActionButton(_ sender: UIButton) {
        useTabBar(selectIndexViewController: Constants.indexOfProgramsVC)
    }
    
    @IBAction func abcActionButton(_ sender: UIButton) {
        refreshGlobalVariables(category: "Abdominals")
        useTabBar(selectIndexViewController: Constants.indexOfWorkoutVC)
    }
    
    @IBAction func foodLogActionButton(_ sender: UIButton) {
        useTabBar(selectIndexViewController: Constants.indexOfAccountVC)
    }
    
    @IBAction func connectActionButton(_ sender: UIButton) {
        useTabBar(selectIndexViewController: Constants.indexOfAboutVC)
    }
}

//MARK: - Constants
extension MainMenuViewController {
    private enum Constants {
        static let indexOfWorkoutVC = 0
        static let indexOfTimerVC = 1
        static let indexOfAccountVC = 2
        static let indexOfProgramsVC = 3
        static let indexOfAboutVC = 4
    }
}
