//
//  Exercises.swift
//  FitnessStudio
//
//  Created by Greenfield
//  Copyright (c) 2017 Greenfield.com. All rights reserved.
//

import UIKit
import Parse
import GoogleMobileAds
import AudioToolbox


// Global vars
var categoryStr = String()
var shoppingArray = [String]()

class Exercises: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UISearchBarDelegate, GADBannerViewDelegate
{
    
    // Views
    @IBOutlet weak var exercisesCollView: UICollectionView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var nothingFoundLabel: UILabel!
    @IBOutlet weak var filterExercises: UIBarButtonItem!
    
    // AdMob Banner View
    let adMobBannerView = GADBannerView()
    
    // Vars
    var exercisesArray = [PFObject]()
    var favArray = [PFObject]()
    var cellSize = CGSize()
 
    
    override func viewWillAppear(_ animated: Bool) {
        
        //filterExercises.tintColor = colorOfBarButtons
        
        if PFUser.current() != nil {
            if categoryStr != "" {
                self.searchBar.text = ""
                if categoryStr == "All Exercises" {
                    categoryStr = ""
                }
                self.queryExercises(categoryStr)

                // Query all Exercise at app startup
            } else {
                if firstStartup {
                    self.queryExercises("")
                    firstStartup = false
                }
            }
        } else {
            let aVC = storyboard?.instantiateViewController(withIdentifier: "Login") as! Login
            present(aVC, animated: true, completion: nil)
        }
        print("Category: \(categoryStr)")

    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Reset search text
        searchBar.frame.origin.y = -60
        searchBar.text = ""
        // Init ad banners
        initAdMobBanner()
        // Set cell size based on current device
        if UIDevice.current.userInterfaceIdiom == .phone {
            // iPhone
            cellSize = CGSize(width: view.frame.size.width - 20, height: 195)
        } else  {
            // iPad
            cellSize = CGSize(width: 450, height: 200)
        }
        // Init ad banners
        initAdMobBanner()
        
    }

    // Query Exercises
    func queryExercises(_ searchText:String) {
        showHUD("Searching...")
        
        let query = PFQuery(className: EXERCISES_CLASS_NAME)
        
        if searchBar.text != "" {
            let keywords = searchText.lowercased().components(separatedBy: " ") as [String]
            query.whereKey(EXERCISES_TITLE_LOWERCASE, contains:  "\(keywords[0])")
        }
        
        if categoryStr != "" {
            query.whereKey(EXERCISES_CATEGORY, equalTo: searchText)
        }
        query.order(byDescending: "createdAt")
        query.findObjectsInBackground { (objects, error)-> Void in
            if error == nil {
                self.exercisesArray = objects!
                self.exercisesCollView.reloadData()
                // Hide/show the nothing found Label
                if self.exercisesArray.count == 0 {
                    self.nothingFoundLabel.isHidden = false
                } else {
                    self.nothingFoundLabel.isHidden = true }
                    self.hideHUD()
            } else {
                self.simpleAlert("\(error!.localizedDescription)")
                self.hideHUD()
            }
        }
    }
    
    // ViewDelegates
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return exercisesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ExercisesCell", for: indexPath) as! ExercisesCell
        
        let exercisesRecord = exercisesArray[indexPath.row]
        
        // Get data
        cell.titleLabel.text = "\(exercisesRecord[EXERCISES_TITLE]!)"
        cell.categoryLabel.text = "\(exercisesRecord[EXERCISES_CATEGORY]!)"
        cell.cookingLabel.text = "\(exercisesRecord[EXERCISES_TIME]!)"
        cell.caloriesLabel.text = "\(exercisesRecord[EXERCISES_RATING]!)"
        cell.coverImage.image = nil
        // Get 1st image
        let imageFile = exercisesRecord[EXERCISES_IMAGE1] as? PFFile
        imageFile?.getDataInBackground(block: { (imageData, error) -> Void in
            if error == nil {
                if let imageData = imageData {
                    cell.coverImage.image = UIImage(data:imageData)
                }
            }
        })
        // Cell's layout
        cell.layer.cornerRadius = 0
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return cellSize
    }
    
    // Tap a Cell then Show Exercises
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        categoryStr = ""
        let exercisesRecord = exercisesArray[indexPath.row]
        
        let rdVC = storyboard?.instantiateViewController(withIdentifier: "ExercisesDetails") as! ExercisesDetails
        rdVC.exercisesObj = exercisesRecord
        navigationController?.pushViewController(rdVC, animated: true)
        
    }
    
    
    
    
    // Search Button
    @IBAction func searchButt(_ sender: AnyObject) {
        showSearchBar()
    }
    
    func showSearchBar() {
        UIView.animate(withDuration: 0.2, delay: 0.0, options: UIViewAnimationOptions.curveLinear, animations: {
            if UIScreen.main.bounds.size.height == 812 {
                // iPhone X
                self.searchBar.frame.origin.y = 84
            } else { self.searchBar.frame.origin.y = 64 }
            
        }, completion: { (finished: Bool) in
            self.searchBar.becomeFirstResponder()
        })
    }
    
    func hideSearchBar() {
        UIView.animate(withDuration: 0.2, delay: 0.0, options: UIViewAnimationOptions.curveLinear, animations: {
            self.searchBar.frame.origin.y = -60
        }, completion: { (finished: Bool) in
            self.searchBar.resignFirstResponder()
        })
    }
    
    
    // SearchBar Delegates
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        categoryStr = ""
        queryExercises(searchBar.text!)
        hideSearchBar()
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        hideSearchBar()
    }
    
    // Filter by Categories
    @IBAction func filterByCategoriesButt(_ sender: AnyObject) {
        searchBar.text = ""
        let catVC = storyboard?.instantiateViewController(withIdentifier: "Categories") as! Categories
        catVC.delegateExercises = self
        present(catVC, animated: true, completion: nil)
    }
    
    @IBAction func backToMainViewController(_ sender: UIBarButtonItem) {
        categoryStr = ""
        self.tabBarController?.navigationController?.popViewController(animated: true)
        self.tabBarController?.navigationController?.isNavigationBarHidden = false
    }
    
    // Refresh Button
    @IBAction func refreshButt(_ sender: AnyObject) {
        searchBar.text = ""
        categoryStr = ""
        if PFUser.current() != nil {
            queryExercises("")
        }
    }
    
    // MARK: - AdMob BANNERS
    func initAdMobBanner() {
        adMobBannerView.adSize =  GADAdSizeFromCGSize(CGSize(width: 320, height: 50))
        adMobBannerView.frame = CGRect(x: 0, y: self.view.frame.size.height, width: 320, height: 50)
        adMobBannerView.adUnitID = ADMOB_BANNER_UNIT_ID
        adMobBannerView.rootViewController = self
        adMobBannerView.delegate = self
        view.addSubview(adMobBannerView)
        let request = GADRequest()
        adMobBannerView.load(request)
    }
    
    
    // Hide the banner
    func hideBanner(_ banner: UIView) {
        UIView.beginAnimations("hideBanner", context: nil)
        
        banner.frame = CGRect(x: 0, y: self.view.frame.size.height, width: banner.frame.size.width, height: banner.frame.size.height)
        UIView.commitAnimations()
        banner.isHidden = true
        
    }
    
    // Show the banner
    func showBanner(_ banner: UIView) {
        var h: CGFloat = 0
        // iPhone X
        if UIScreen.main.bounds.size.height == 812 { h = 84
        } else { h = 48 }
        
        UIView.beginAnimations("showBanner", context: nil)
        banner.frame = CGRect(x: view.frame.size.width/2 - banner.frame.size.width/2,
                              y: view.frame.size.height - banner.frame.size.height - h,
                              width: banner.frame.size.width, height: banner.frame.size.height);
        UIView.commitAnimations()
        banner.isHidden = false
    }
    
    
    
    
    // AdMob banner available
    func adViewDidReceiveAd(_ view: GADBannerView) {
        print("AdMob loaded!")
        showBanner(adMobBannerView)
    }
    
    // NO AdMob banner available
    func adView(_ view: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print("AdMob Can't load ads right now, they'll be available later \n\(error)")
        hideBanner(adMobBannerView)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
