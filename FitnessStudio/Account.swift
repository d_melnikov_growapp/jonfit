//
//  Account.swift
//  FitnessStudio
//
//  Created by Greenfield
//  Copyright (c) 2017 Greenfield.com. All rights reserved.
//

import UIKit
import Parse
import GoogleMobileAds

class PurchasedExercisesCell: UITableViewCell {
    // Views
    @IBOutlet weak var coverThumbnail: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
}

// Account
class Account: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, GADBannerViewDelegate {
    // Views
    @IBOutlet weak var noUserView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var favExercisesCollView: UICollectionView!
    
    @IBOutlet weak var rightBarButtonItem: UIBarButtonItem!
    
    //Ad banners properties
    var adMobBannerView = GADBannerView()
    
    // Vars
    var purchasedArray = [PFObject]()
    var favExercisesArray = [PFObject]()
    var cellSize = CGSize()
    
    override func viewWillAppear(_ animated: Bool) {
        if PFUser.current() != nil {
            self.noUserView.isHidden = true
            self.mainView.isHidden = false
            // Call query
            self.queryFavoriteExercises()
            self.fullNameLabel.text = "\(PFUser.current()![USER_FULLNAME]!)"
        } else {
            let aVC = storyboard?.instantiateViewController(withIdentifier: "Login") as! Login
            present(aVC, animated: true, completion: nil)
        }
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Custom TopBar Elements Right & left
        if let font : UIFont = UIFont(name: "Helvetica Neue", size: 12) {
            rightBarButtonItem.setTitleTextAttributes([NSAttributedStringKey.font: font], for: UIControlState.normal)
        }
        // Set cell size based on current device
        if UIDevice.current.userInterfaceIdiom == .phone {
            // iPhone
            cellSize = CGSize(width: view.frame.size.width/2 - 20, height: 195)
        } else  {
            // iPad
            cellSize = CGSize(width: view.frame.size.width/3 - 20, height: 218)
        }
        // Init ad banners
        initAdMobBanner()
        
    }

    // Query Favorites Exercises
    func queryFavoriteExercises() {
        favExercisesCollView.isHidden = false
        favExercisesArray.removeAll()
        showHUD("Favorites...")
        
        let query = PFQuery(className: FAV_CLASS_NAME)
        query.whereKey(FAV_FAVORITED_BY, equalTo: PFUser.current()!)
        query.findObjectsInBackground { (objects, error)-> Void in
            if error == nil {
                self.favExercisesArray = objects!
                self.favExercisesCollView.reloadData()
                self.hideHUD()
                
            } else {
                self.simpleAlert("\(error!.localizedDescription)")
                self.hideHUD()
            }}
    }
    
    // Favorites
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return favExercisesArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ExercisesCell", for: indexPath) as! ExercisesCell
        
        let favClass = favExercisesArray[indexPath.row]
        
        // Get Exercises Pointer
        var exercisesPointer = favClass[FAV_RECIPE_POINTER] as! PFObject
        do { exercisesPointer = try exercisesPointer.fetchIfNeeded() } catch {}
        
        // Get Title and Category
        cell.titleLabel.text = "\(exercisesPointer[EXERCISES_TITLE]!)"
        cell.categoryLabel.text = "\(exercisesPointer[EXERCISES_CATEGORY]!)"
        cell.coverImage.image = nil
        // Get 1st image
        let imageFile = exercisesPointer[EXERCISES_IMAGE1] as? PFFile
        imageFile?.getDataInBackground(block: { (imageData, error) -> Void in
            if error == nil {
                if let imageData = imageData {
                    cell.coverImage.image = UIImage(data:imageData)
                }
            }
        })
        
        cell.favOutlet.tag = indexPath.row
        
        // Customize cell's layout
        cell.layer.cornerRadius = 0
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOffset = CGSize(width: 10, height: 10)
        cell.layer.shadowOpacity = 1

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return cellSize
    }
    
    // Tap a Cell = Show Favorites
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let favClass = favExercisesArray[indexPath.row]
        
        // Get Recipe Pointer
        var exercisesPointer = favClass[FAV_RECIPE_POINTER] as! PFObject
        do { exercisesPointer = try exercisesPointer.fetchIfNeeded() } catch {}
        
        let rdVC = self.storyboard?.instantiateViewController(withIdentifier: "ExercisesDetails") as! ExercisesDetails
        rdVC.exercisesObj = exercisesPointer
        self.navigationController?.pushViewController(rdVC, animated: true)
    }

    // Delete Favorites Button
    @IBAction func favButt(_ sender: AnyObject) {
        let butt = sender as! UIButton
        
        let favClass = favExercisesArray[butt.tag]
        
        favClass.deleteInBackground { (succ, error) in
            if error == nil {
                self.simpleAlert("Removed from your Favorites")
                self.favExercisesArray.remove(at: butt.tag)
                self.favExercisesCollView.reloadData()
                
            } else {
                self.simpleAlert("\(error!.localizedDescription)")
            }
        }
        
    }

    // Login Button
    @IBAction func loginButt(_ sender: AnyObject) {
        let loginVC = storyboard?.instantiateViewController(withIdentifier: "Login") as! Login
        present(loginVC, animated: true, completion: nil)
    }

    // Logout Button
    @IBAction func logoutButt(_ sender: AnyObject) {
        let alert = UIAlertController(title: APP_NAME,
                                      message: "Are you sure you want to logout?",
                                      preferredStyle: UIAlertControllerStyle.alert)
        
        let ok = UIAlertAction(title: "Logout", style: UIAlertActionStyle.default, handler: { (action) -> Void in
            self.showHUD("Logout...")
            
            PFUser.logOutInBackground { (error) -> Void in
                if error == nil {
                    // Show the Login screen
                    let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "Login") as! Login
                    self.present(loginVC, animated: true, completion: nil)
                }
                self.hideHUD()
            }
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: { (action) -> Void in })
        
        alert.addAction(ok); alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func backToMainView(_ sender: UIBarButtonItem) {
        categoryStr = ""
        self.tabBarController?.navigationController?.popViewController(animated: true)
        self.tabBarController?.navigationController?.isNavigationBarHidden = false
    }

    // MARK: - IAD + ADMOB BANNER METHODS
    func initAdMobBanner() {
        adMobBannerView.adSize =  GADAdSizeFromCGSize(CGSize(width: 320, height: 50))
        adMobBannerView.frame = CGRect(x: 0, y: self.view.frame.size.height, width: 320, height: 50)
        adMobBannerView.adUnitID = ADMOB_BANNER_UNIT_ID
        adMobBannerView.rootViewController = self
        adMobBannerView.delegate = self
        view.addSubview(adMobBannerView)
        let request = GADRequest()
        adMobBannerView.load(request)
    }
    
    
    // Hide the banner
    func hideBanner(_ banner: UIView) {
        UIView.beginAnimations("hideBanner", context: nil)
        
        banner.frame = CGRect(x: 0, y: self.view.frame.size.height, width: banner.frame.size.width, height: banner.frame.size.height)
        UIView.commitAnimations()
        banner.isHidden = true
        
    }
    
    // Show the banner
    func showBanner(_ banner: UIView) {
        UIView.beginAnimations("showBanner", context: nil)
        banner.frame = CGRect(x: view.frame.size.width/2 - banner.frame.size.width/2,
                              y: view.frame.size.height - banner.frame.size.height,
                              width: banner.frame.size.width, height: banner.frame.size.height);
        UIView.commitAnimations()
        banner.isHidden = false
    }
 
    // AdMob banner available
    func adViewDidReceiveAd(_ view: GADBannerView) {
        print("AdMob loaded!")
        showBanner(adMobBannerView)
    }
    
    // NO AdMob banner available
    func adView(_ view: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print("AdMob Can't load ads right now, they'll be available later \n\(error)")
        hideBanner(adMobBannerView)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
