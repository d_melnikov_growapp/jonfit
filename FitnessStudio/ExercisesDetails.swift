//
//  ExercisesDetails.swift
//  FitnessStudio
//
//  Created by Greenfield
//  Copyright (c) 2017 Greenfield.com. All rights reserved.
//

import UIKit
import Parse
import GoogleMobileAds
import AudioToolbox
import MessageUI


class ExercisesDetails: UIViewController, UIScrollViewDelegate, GADBannerViewDelegate
{
    
    // Views
    @IBOutlet weak var sliderScrollView: UIScrollView!
    @IBOutlet weak var image1: UIImageView!
    @IBOutlet weak var image2: UIImageView!
    @IBOutlet weak var image3: UIImageView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var containerScrollView: UIScrollView!
    @IBOutlet weak var exercisesView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var aboutExercisesLabel: UILabel!
    @IBOutlet weak var difficultyLabel: UILabel!
    @IBOutlet weak var cookingLabel: UILabel!
    @IBOutlet weak var portionsLabel: UILabel!
    @IBOutlet weak var caloriesLabel: UILabel!
    @IBOutlet weak var glutenFreeLabel: UILabel!
    @IBOutlet var timingLabels: [UILabel]!
    @IBOutlet weak var videoWebView: UIWebView!
    @IBOutlet weak var videoTitleLabel: UILabel!
    @IBOutlet weak var ingredientsTxt: UITextView!
    @IBOutlet weak var addToShoppingOutlet: UIButton!
    @IBOutlet weak var preparationTxt: UITextView!
    @IBOutlet weak var favOutlet: UIButton!
    @IBOutlet weak var openYouTubeOutlet: UIButton!
    
    
    // AdMob Banner View
    let adMobBannerView = GADBannerView()
    
    // Vars
    var exercisesObj = PFObject(className: EXERCISES_CLASS_NAME)
    var ingredientsArray = [String]()
    var favoritesArray = [PFObject]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.edgesForExtendedLayout = UIRectEdge()
    
        
        // Setup slider ScrollView
        image1.frame.origin.x = 0
        image2.frame.origin.x = sliderScrollView.frame.size.width
        image3.frame.origin.x = sliderScrollView.frame.size.width*2
        
        
        // Back BarButton Item
        let backButt = UIButton(type: UIButtonType.custom)
        backButt.adjustsImageWhenHighlighted = false
        backButt.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButt.addTarget(self, action: #selector(backButton(_:)), for: .touchUpInside)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButt)
        backButt.setTitle("Back", for: UIControlState.normal)
        backButt.titleLabel?.font = UIFont(name: "HelveticaNeue-Light", size: 17)
        backButt.setTitleColor(UIColor.white, for:UIControlState.normal)
                
        // SHARE BarButton Item
        let shareButt = UIButton(type: UIButtonType.custom)
        shareButt.adjustsImageWhenHighlighted = false
        shareButt.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        shareButt.addTarget(self, action: #selector(shareButton(_:)), for: .touchUpInside)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: shareButt)
        shareButt.setTitle("Share", for: UIControlState.normal)
        shareButt.titleLabel?.font = UIFont(name: "HelveticaNeue-Light", size: 17)
        shareButt.setTitleColor(UIColor.white, for:UIControlState.normal)
        
        // Show Exercise details
        showExercisesDetails()
        
        // Init ad banners
        initAdMobBanner()
    }
    
    
    // Show Exercise Details
    func showExercisesDetails() {
        
        if PFUser.current() != nil {
            let query = PFQuery(className: FAV_CLASS_NAME)
            query.whereKey(FAV_FAVORITED_BY, equalTo: PFUser.current()!)
            query.whereKey(FAV_RECIPE_POINTER, equalTo: exercisesObj)
            query.findObjectsInBackground { (objects, error)-> Void in
                if error == nil {
                    if objects?.count != 0 {
                        self.favOutlet.setBackgroundImage(UIImage(named: "favedButt"), for: .normal)
                    }
                } else { self.simpleAlert("\(error!.localizedDescription)") }
            }
        }
        
        // Get images
        let imageFile1 = exercisesObj[EXERCISES_IMAGE1] as? PFFile
        imageFile1?.getDataInBackground(block: { (imageData, error) -> Void in
            if error == nil {
                if let imageData = imageData {
                    self.image1.image = UIImage(data:imageData)
                    self.pageControl.numberOfPages = 1
                }}})
        
        let imageFile2 = exercisesObj[EXERCISES_IMAGE2] as? PFFile
        imageFile2?.getDataInBackground(block: { (imageData, error) -> Void in
            if error == nil {
                if let imageData = imageData {
                    self.image2.image = UIImage(data:imageData)
                    self.pageControl.numberOfPages = 2
                }}})
        
        
        let imageFile3 = exercisesObj[EXERCISES_IMAGE3] as? PFFile
        imageFile3?.getDataInBackground(block: { (imageData, error) -> Void in
            if error == nil {
                if let imageData = imageData {
                    self.image3.image = UIImage(data:imageData)
                    self.pageControl.numberOfPages = 3
                }}})
        
        sliderScrollView.contentSize = CGSize(width: sliderScrollView.frame.size.width * CGFloat(pageControl.numberOfPages), height: sliderScrollView.frame.size.height)
        
        
        
        // Show Exercise Details
        titleLabel.text = "\(exercisesObj[EXERCISES_TITLE]!)"
        categoryLabel.text = "\(exercisesObj[EXERCISES_CATEGORY]!)"
        categoryLabel.layer.cornerRadius = 8
        categoryLabel.layer.borderColor = UIColor.white.cgColor
        categoryLabel.layer.backgroundColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.95).cgColor
        
        aboutExercisesLabel.text = "\(exercisesObj[EXERCISES_ABOUT]!)"
        
        //Line
        let paragraphStyle02 = NSMutableParagraphStyle()
        let lineHeight02: CGFloat = 18.0
        paragraphStyle02.lineHeightMultiple = lineHeight02
        paragraphStyle02.maximumLineHeight = lineHeight02
        paragraphStyle02.minimumLineHeight = lineHeight02
        let ats02 = [NSAttributedStringKey.font: UIFont(name: "HelveticaNeue-Light", size: 14.0)!, NSAttributedStringKey.paragraphStyle: paragraphStyle02]
        aboutExercisesLabel.attributedText = NSAttributedString(string: "\(exercisesObj[EXERCISES_ABOUT]!)", attributes: ats02)
        
        difficultyLabel.text = "\(exercisesObj[EXERCISES_ABILITY]!)"
        difficultyLabel.layer.cornerRadius = 8
        difficultyLabel.layer.borderColor = UIColor.white.cgColor
        difficultyLabel.layer.backgroundColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.95).cgColor
        
        cookingLabel.text = "\(exercisesObj[EXERCISES_TIME]!)"
        portionsLabel.text = "\(exercisesObj[EXERCISES_EQUIPMENT]!)"
        caloriesLabel.text = "\(exercisesObj[EXERCISES_RATING]!)"
        glutenFreeLabel.text = "\(exercisesObj[EXERCISES_TYPE]!)"
        
        
        // Get Youtube Video
        if exercisesObj[EXERCISES_YOUTUBE] != nil {
            if "\(exercisesObj[EXERCISES_YOUTUBE]!)" != "" {
                let youtubeLink = "\(exercisesObj[EXERCISES_YOUTUBE]!)"
                let videoId = youtubeLink.replacingOccurrences(of: "https://vimeo.com/", with: "")
                let embedHTML = "<iframe width='\(videoWebView.frame.size.width)' height='\(videoWebView.frame.size.height)' src='https://player.vimeo.com/video/\(videoId)?rel=0&allowfullscreen=1;controls=0&amp;showinfo=0' frameborder='0'</iframe>"
                
                videoWebView.loadHTMLString(embedHTML, baseURL: nil)
                
            } else {
                videoWebView.isHidden = true
            }
            
        } else {
            videoWebView.isHidden = true
        }
        
        if exercisesObj[EXERCISES_VIDEO_TITLE] != nil { videoTitleLabel.text = "\(exercisesObj[EXERCISES_VIDEO_TITLE]!)"
        } else { videoTitleLabel.text = "No video Available" }
        
        
        
        // Line Spacing - 01
        let paragraphStyle01 = NSMutableParagraphStyle()
        let lineHeight01: CGFloat = 18.0
        paragraphStyle01.lineHeightMultiple = lineHeight01
        paragraphStyle01.maximumLineHeight = lineHeight01
        paragraphStyle01.minimumLineHeight = lineHeight01
        _ = [NSAttributedStringKey.font: UIFont(name: "HelveticaNeue-Light", size: 14.0)!, NSAttributedStringKey.paragraphStyle: paragraphStyle01]
        
        
        // Get Steps
        preparationTxt.text = "\(exercisesObj[EXERCISES_STEPS]!)"
        preparationTxt.sizeToFit()
        preparationTxt.frame.size.height = preparationTxt.frame.size.height + 400
        
        
        // Line Spacing - 02
        let paragraphStyle = NSMutableParagraphStyle()
        let lineHeight: CGFloat = 18.5
        paragraphStyle.lineHeightMultiple = lineHeight
        paragraphStyle.maximumLineHeight = lineHeight
        paragraphStyle.minimumLineHeight = lineHeight
        let ats = [NSAttributedStringKey.font: UIFont(name: "HelveticaNeue-Light", size: 14.0)!, NSAttributedStringKey.paragraphStyle: paragraphStyle]
        preparationTxt.attributedText = NSAttributedString(string: "\(exercisesObj[EXERCISES_STEPS]!)", attributes: ats)
        
        
        // ScrollView content size
        self.exercisesView.frame.size.height = self.preparationTxt.frame.size.height + self.preparationTxt.frame.origin.y + self.exercisesView.frame.origin.y - 150
        self.containerScrollView.contentSize = CGSize(width: self.containerScrollView.frame.size.width, height: self.exercisesView.frame.size.height + self.exercisesView.frame.origin.y)
        
    }
    
    
    // ScrollView Delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageWidth = sliderScrollView.frame.size.width
        let page = Int(floor((sliderScrollView.contentOffset.x * 2 + pageWidth) / (pageWidth * 2)))
        pageControl.currentPage = page
    }
    
    

    
    // Open Youtube Button
    @IBAction func openYouTubeButt(_ sender: AnyObject) {
        let youtubeLink = "\(exercisesObj[EXERCISES_YOUTUBE]!)"
        let MY_URL = URL(string: youtubeLink)
        UIApplication.shared.openURL(MY_URL!)
    }
    

    
    
    // Favorite Exercise Button
    @IBAction func favoriteButt(_ sender: AnyObject) {
        // USER IS LOGGED IN
        if PFUser.current() != nil {
            
            let query = PFQuery(className: FAV_CLASS_NAME)
            query.whereKey(FAV_FAVORITED_BY, equalTo: PFUser.current()!)
            query.whereKey(FAV_RECIPE_POINTER, equalTo: exercisesObj)
            query.findObjectsInBackground { (objects, error)-> Void in
                if error == nil {
                    self.favoritesArray = objects!
                    print("FAV ARRAY: \(self.favoritesArray)")
                    
                    
                    var favClass = PFObject(className: FAV_CLASS_NAME)
                    
                    // Favorite Exercise
                    if self.favoritesArray.count == 0 {
                        favClass[FAV_FAVORITED_BY] = PFUser.current()!
                        favClass[FAV_RECIPE_POINTER] = self.exercisesObj
                        
                        favClass.saveInBackground { (success, error) -> Void in
                            if error == nil {
                                self.simpleAlert("Added this exercise to your Favorites")
                                self.favOutlet.setBackgroundImage(UIImage(named: "favedButt"), for: UIControlState())
                            } else {
                                self.simpleAlert("\(error!.localizedDescription)")
                            }
                        }
                        
                        
                        //  Delete Exercise
                    } else if self.favoritesArray.count > 0 {
                        favClass = self.favoritesArray[0]
                        favClass.deleteInBackground(block: { (succ, error) in
                            if error == nil {
                                self.simpleAlert("Removed this exercise from your Favorites")
                                self.favOutlet.setBackgroundImage(UIImage(named: "favButt"), for: UIControlState())
                            } else {
                                self.simpleAlert("\(error!.localizedDescription)")
                            }
                        })
                    }
                    
                    // error
                } else {
                    self.simpleAlert("\(error!.localizedDescription)")
                }}
            
            
            
            
            // If User is not Logged or Register
        } else {
            let alert = UIAlertController(title: APP_NAME,
                                          message: "You must Login or Signup to favorite a exercise",
                                          preferredStyle: UIAlertControllerStyle.alert)
            
            let ok = UIAlertAction(title: "Login", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "Login") as! Login
                self.present(loginVC, animated: true, completion: nil)
            })
            
            let cancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: { (action) -> Void in })
            
            alert.addAction(ok); alert.addAction(cancel)
            present(alert, animated: true, completion: nil)
        }
        
    }
    
    // Share Button
    @objc func shareButton(_ sender:UIButton) {
        let messageStr  = "I love this exercise: \(exercisesObj[EXERCISES_TITLE]!), found on #\(APP_NAME)"
        let image = image1.image
        
        let shareItems = [messageStr, image!] as [Any]
        
        let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
        activityViewController.excludedActivityTypes = [UIActivityType.print, UIActivityType.postToWeibo, UIActivityType.copyToPasteboard, UIActivityType.addToReadingList, UIActivityType.postToVimeo]
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            // iPad
            let popOver = UIPopoverController(contentViewController: activityViewController)
            popOver.present(from: CGRect.zero, in: view, permittedArrowDirections: UIPopoverArrowDirection.any, animated: true)
        } else {
            // iPhone
            self.present(activityViewController, animated: true, completion: nil)
        }
    }
    
    // Back Button
    @objc func backButton(_ sender:UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }

    
    
    // MARK: - AdMob BANNERS
    func initAdMobBanner() {
        adMobBannerView.adSize =  GADAdSizeFromCGSize(CGSize(width: 320, height: 50))
        adMobBannerView.frame = CGRect(x: 0, y: self.view.frame.size.height, width: 320, height: 50)
        adMobBannerView.adUnitID = ADMOB_BANNER_UNIT_ID
        adMobBannerView.rootViewController = self
        adMobBannerView.delegate = self
        view.addSubview(adMobBannerView)
        let request = GADRequest()
        adMobBannerView.load(request)
    }
    
    
    // Hide the banner
    func hideBanner(_ banner: UIView) {
        UIView.beginAnimations("hideBanner", context: nil)
        
        banner.frame = CGRect(x: 0, y: self.view.frame.size.height, width: banner.frame.size.width, height: banner.frame.size.height)
        UIView.commitAnimations()
        banner.isHidden = true
        
    }
    
    // Show the banner
    func showBanner(_ banner: UIView) {
        UIView.beginAnimations("showBanner", context: nil)
        banner.frame = CGRect(x: view.frame.size.width/2 - banner.frame.size.width/2,
                              y: view.frame.size.height - banner.frame.size.height,
                              width: banner.frame.size.width, height: banner.frame.size.height);
        UIView.commitAnimations()
        banner.isHidden = false
    }
    
    
    
    // AdMob banner available
    func adViewDidReceiveAd(_ view: GADBannerView) {
        print("AdMob loaded!")
        showBanner(adMobBannerView)
    }
    
    // NO AdMob banner available
    func adView(_ view: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print("AdMob Can't load ads right now, they'll be available later \n\(error)")
        hideBanner(adMobBannerView)
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
