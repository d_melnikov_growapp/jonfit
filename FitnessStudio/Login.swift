//
//  Login.swift
//  FitnessStudio
//
//  Created by Greenfield
//  Copyright (c) 2017 Greenfield.com. All rights reserved.
//



import UIKit
import Parse
import ParseFacebookUtilsV4


class Login: UIViewController, UITextFieldDelegate, UIAlertViewDelegate
{
    
    // Views
    @IBOutlet var containerScrollView: UIScrollView!
    @IBOutlet var usernameTxt: UITextField!
    @IBOutlet var passwordTxt: UITextField!
    @IBOutlet weak var facebookOutlet: UIButton!
    @IBOutlet weak var fbOutlet: UIButton!
    
    var userEmail = String()
    
    
    override func viewWillAppear(_ animated: Bool) {
        if PFUser.current() != nil {
            dismiss(animated: false, completion: nil)
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup layouts
        containerScrollView.contentSize = CGSize(width: containerScrollView.frame.size.width, height: 550)
        
        // Change placeholder's color
        let color = UIColor.gray
        usernameTxt.attributedPlaceholder = NSAttributedString(string: "Your Email Address", attributes: [NSAttributedStringKey.foregroundColor: color])
        passwordTxt.attributedPlaceholder = NSAttributedString(string: "Your Password", attributes: [NSAttributedStringKey.foregroundColor: color])
        
    }
    
    
    
    // Login Button
    @IBAction func loginButt(_ sender: AnyObject) {
        passwordTxt.resignFirstResponder()
        showHUD("Loading...")
        
        PFUser.logInWithUsername(inBackground: usernameTxt.text!, password:passwordTxt.text!) { (user, error) -> Void in
            // Login successfull
            if user != nil {
                self.dismiss(animated: true, completion: nil)
                hudView.removeFromSuperview()
                
                // Login failed. Try again or SignUp
            } else {
                self.simpleAlert("\(error!.localizedDescription)")
                hudView.removeFromSuperview()
            }}
    }
    
    
    // Facebook Login Button
    @IBAction func facebookButt(_ sender: Any) {
        let permissions = ["public_profile", "email"];
        showHUD("SignUp...")
        
        // Login PFUser using Facebook
        PFFacebookUtils.logInInBackground(withReadPermissions: permissions) { (user, error) in
            if user == nil {
                self.simpleAlert("Facebook login cancelled")
                self.hideHUD()
                
            } else if (user!.isNew) {
                print("new user signed up and logged in through Facebook");
                self.getFBUserData()
                
            } else {
                print("user logged in through Facebook");
                
                self.dismiss(animated: true, completion: nil)
                self.hideHUD()
            }
            
            if error != nil {
                self.simpleAlert("\(error!.localizedDescription)")
            }}
    }
    
    
    func getFBUserData() {
        let graphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, email, picture.type(large)"])
        let connection = FBSDKGraphRequestConnection()
        connection.add(graphRequest) { (connection, result, error) in
            if error == nil {
                let userData:[String:AnyObject] = result as! [String : AnyObject]
                
                // Get data
                let facebookID = userData["id"] as! String
                let name = userData["name"] as! String
                var email = ""
                if userData["email"] != nil { email = userData["email"] as! String
                } else { email = "noemail@facebook.com" }
                
                
                // Get avatar
                let currUser = PFUser.current()!
                
                let pictureURL = URL(string: "https://graph.facebook.com/\(facebookID)/picture?type=large")
                let urlRequest = URLRequest(url: pictureURL!)
                let session = URLSession.shared
                let dataTask = session.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
                    if error == nil && data != nil {
                        let image = UIImage(data: data!)
                        let imageData = UIImageJPEGRepresentation(image!, 0.8)
                        let imageFile = PFFile(name:"avatar.jpg", data:imageData!)
                        currUser[USER_AVATAR] = imageFile
                        currUser.saveInBackground(block: { (succ, error) in
                            print("...avatar saved")
                            self.hideHUD()
                            self.dismiss(animated: true, completion: nil)
                        })
                    } else {
                        self.simpleAlert("\(error!.localizedDescription)")
                        self.hideHUD()
                    }})
                dataTask.resume()
                
                
                // Update user data
                let nameArr = name.components(separatedBy: " ")
                var username = String()
                for word in nameArr {
                    username.append(word.lowercased())
                }
                currUser.username = username
                currUser.email = email
                currUser[USER_FULLNAME] = name
                currUser.saveInBackground(block: { (succ, error) in
                    if error == nil {
                        print("users updated...")
                    }})
                
                
            } else {
                self.simpleAlert("\(error!.localizedDescription)")
                self.hideHUD()
            }}
        connection.start()
    }
    
    
    // SignUp Button
    @IBAction func signupButt(_ sender: AnyObject) {
        let signupVC = self.storyboard?.instantiateViewController(withIdentifier: "SignUp") as! SignUp
        present(signupVC, animated: true, completion: nil)
    }
    
    
    
    
    // Textfields Delegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == usernameTxt  {  passwordTxt.becomeFirstResponder() }
        if textField == passwordTxt  {  passwordTxt.resignFirstResponder() }
        return true
    }
    
    
    // Tap to Dismiss Keyboard
    @IBAction func tapToDismissKeyboard(_ sender: UITapGestureRecognizer) {
        dismissKeyboard()
    }
    func dismissKeyboard() {
        usernameTxt.resignFirstResponder()
        passwordTxt.resignFirstResponder()
    }
    
    
    
    
    // Forgot Password
    @IBAction func forgotPasswButt(_ sender: AnyObject) {
        let alert = UIAlertController(title: APP_NAME,
                                      message: "Type your email address you used to register.",
                                      preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "Reset Password", style: .default, handler: { (action) -> Void in
            let textField = alert.textFields!.first!
            let txtStr = textField.text!
            
            PFUser.requestPasswordResetForEmail(inBackground: txtStr, block: { (succ, error) in
                self.simpleAlert("You will receive an email shortly with a link to reset your password")
            })
        })
        
        // Cancel button
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) -> Void in })
        
        // Add textField
        alert.addTextField(configurationHandler: { (textField: UITextField) in
            textField.keyboardAppearance = .dark
            textField.keyboardType = .emailAddress
        })
        
        alert.addAction(ok)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}


