//
//  Configs.swift
//  FitnessStudio
//
//  Created by Greenfield
//  Copyright (c) 2017 Greenfield.com. All rights reserved.
//

import Foundation
import UIKit
import Parse

let APP_NAME = "JonFit" // Your App Name

let ADMOB_BANNER_UNIT_ID = "" // Your AdMob ID

// Yor Details for About Us
let ADMIN_EMAIL_ADDRESS = "Jon@yourdomain.com"
let WEBSITE_URL = "http://www.jonbrownell.com"
let FACEBOOK_URL = "https://www.facebook.com/JonBrownell21/"
let TWITTER_URL = "https://twitter.com/jonbrownell_"


// Your Parse Keys
let PARSE_APP_KEY = "MXmokjZeM3XYeURZIaO8R3APckSORKoAUgp1uAfl"
let PARSE_CLIENT_KEY = "p4JYfpjnx1hvGBw00Nr3SowBNs0oBfzK2YN1GUsv"

// Exercises Categories
let categoriesArray =  [
    "All Exercises",
    "Abdominals",
    "Back",
    "Biceps",
    "Calves",
    "Chest",
    "Glutes",
    "Hamstrings",
    "Quadriceps",
    "Shoulders",
    "Traps",
    "Fun Cardio",
    "Healthy Recipes",
    "Home Workouts",
    "Partner Workouts",
    "Shredder Tips",
    "Motivation"
    
    
    // You can add categories here...
    // IMPORTANT: Also remember to add the proper images into the EXERCISES CATEGORIES folder in Assets.xcassets, naming them exactly like the red strings above!
    
]



// Custom color
let yellow = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0)



// HUD VIEW
let hudView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
let label = UILabel()
let indicatorView = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
extension UIViewController {
    func showHUD(_ message:String) {
        hudView.center = CGPoint(x: view.frame.size.width/2, y: view.frame.size.height/2)
        hudView.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1)
        hudView.alpha = 0.7
        hudView.layer.cornerRadius = 8
        
        indicatorView.center = CGPoint(x: hudView.frame.size.width/2, y: hudView.frame.size.height/2)
        indicatorView.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        hudView.addSubview(indicatorView)
        indicatorView.startAnimating()
        view.addSubview(hudView)
        
        label.frame = CGRect(x: 0, y: 70, width: 100, height: 30)
        label.font = UIFont(name: "HelveticaNeue-Light", size: 13)
        label.text = message
        label.textAlignment = .center
        label.textColor = UIColor.white
        hudView.addSubview(label)
    }
    
    func hideHUD() {
        hudView.removeFromSuperview()
        label.removeFromSuperview()
    }
    
    func simpleAlert(_ mess:String) {
        UIAlertView(title: APP_NAME, message: mess, delegate: nil, cancelButtonTitle: "OK").show()
    }
}








// Vars for Parse
let defaults = UserDefaults.standard
var justSignedUp = false


let USER_CLASS_NAME = "_User"
let USER_FULLNAME = "fullName"
let USER_AVATAR = "avatar"
let USER_USERNAME = "username"
let USER_PASSWORD = "password"
let USER_EMAIL = "email"

let EXERCISES_CLASS_NAME = "Exercises"
let EXERCISES_TITLE = "title"     // String
let EXERCISES_IMAGE1 = "image1"   // File
let EXERCISES_IMAGE2 = "image2"   // File
let EXERCISES_IMAGE3 = "image3"   // File
let EXERCISES_TITLE_LOWERCASE = "titleLowercase" // String
let EXERCISES_CATEGORY = "category"   // String
let EXERCISES_ABOUT = "aboutExercises"   // String
let EXERCISES_ABILITY = "ability"   // String
let EXERCISES_TIME = "time"     // String
let EXERCISES_EQUIPMENT = "equipment"   // String
let EXERCISES_RATING = "rating"   // String
let EXERCISES_TYPE = "type"  // String
let EXERCISES_YOUTUBE = "youtube"     // String
let EXERCISES_VIDEO_TITLE = "videoTitle"  // String
let EXERCISES_STEPS = "steps" // String

let FAV_CLASS_NAME = "Favorites"
let FAV_FAVORITED_BY = "favoritedBy"
let FAV_RECIPE_POINTER = "recipePointer"



