//
//  SignUp.swift
//  FitnessStudio
//
//  Created by Greenfield
//  Copyright (c) 2017 Greenfield.com. All rights reserved.
//



import UIKit
import Parse


class SignUp: UIViewController, UITextFieldDelegate
{
    
    // Views
    @IBOutlet var containerScrollView: UIScrollView!
    @IBOutlet var usernameTxt: UITextField!
    @IBOutlet var passwordTxt: UITextField!
    @IBOutlet var fullnameTxt: UITextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup layout views
        containerScrollView.contentSize = CGSize(width: containerScrollView.frame.size.width, height: 550)
        
        // Change placeholder's color
        let color = UIColor.gray
        usernameTxt.attributedPlaceholder = NSAttributedString(string: "Type your Email Address", attributes: [NSAttributedStringKey.foregroundColor: color])
        passwordTxt.attributedPlaceholder = NSAttributedString(string: "Type a Password", attributes: [NSAttributedStringKey.foregroundColor: color])
        fullnameTxt.attributedPlaceholder = NSAttributedString(string: "Type your Fullname", attributes: [NSAttributedStringKey.foregroundColor: color])
        
    }
    
    
    // Tap to Dismiss Keyboard
    @IBAction func tapToDismissKeyboard(_ sender: UITapGestureRecognizer) {
        dismissKeyboard()
    }
    func dismissKeyboard() {
        usernameTxt.resignFirstResponder()
        passwordTxt.resignFirstResponder()
        fullnameTxt.resignFirstResponder()
    }
    
    
    
    // SignUp Button
    @IBAction func signupButt(_ sender: AnyObject) {
        dismissKeyboard()
        showHUD("SignUp...")
        
        let userForSignUp = PFUser()
        userForSignUp.username = usernameTxt.text!.lowercased()
        userForSignUp.password = passwordTxt.text
        userForSignUp.email = usernameTxt.text
        userForSignUp[USER_FULLNAME] = fullnameTxt.text
        
        // Save default avatar
        let imageData = UIImageJPEGRepresentation(UIImage(named:"logo")!, 1.0)
        let imageFile = PFFile(name:"image.jpg", data:imageData!)
        userForSignUp[USER_AVATAR] = imageFile
        
        if usernameTxt.text == "" || passwordTxt.text == "" || fullnameTxt.text == "" {
            simpleAlert("Please fill all fields to SignUp on \(APP_NAME)")
            hideHUD()
            
        } else {
            userForSignUp.signUpInBackground { (succeeded, error) -> Void in
                if error == nil {
                    self.dismiss(animated: false, completion: nil)
                    self.hideHUD()
                    
                } else {
                    self.simpleAlert("\(error!.localizedDescription)")
                    self.hideHUD()
                }}
        }
        
    }
    
    
    
    
    
    // Textfields Delegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == usernameTxt {  passwordTxt.becomeFirstResponder()  }
        if textField == passwordTxt {  fullnameTxt.becomeFirstResponder()     }
        if textField == fullnameTxt    {  fullnameTxt.resignFirstResponder()     }
        return true
    }
    
    
    
    // Back Button
    @IBAction func backButt(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    
    
    
    // Terms of Use Button
    @IBAction func touButt(_ sender: AnyObject) {
        let touVC = self.storyboard?.instantiateViewController(withIdentifier: "TermsOfUse") as! TermsOfUse
        present(touVC, animated: true, completion: nil)
    }
    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}


