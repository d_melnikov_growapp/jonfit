//
//  Products.swift
//  FitnessStudio
//
//  Created by Денис on 05/12/2018.
//  Copyright © 2018 GF. All rights reserved.
//

import Foundation

public struct Products {
    
    public static let SwiftShopping = "com.growapp.givi"
    
    private static let productIdentifiers: Set<ProductIdentifier> = {
        var productIdentifiers = Set<ProductIdentifier>()
        if let url = Bundle.main.url(forResource: "product_ids", withExtension: "plist") {
            if let array = NSArray(contentsOf: url) {
                for product_id in array {
                    productIdentifiers.insert(product_id as! String)
                }
            }
        }
        return productIdentifiers
    }()
    
    public static let store = IAPHelper(productIds: Products.productIdentifiers)
}

func resourceNameForProductIdentifier(_ productIdentifier: String) -> String? {
    return productIdentifier.components(separatedBy: ".").last
}
