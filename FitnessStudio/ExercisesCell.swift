//
//  ExercisesCell.swift
//  FitnessStudio
//
//  Created by Greenfield
//  Copyright (c) 2017 Greenfield.com. All rights reserved.
//

import UIKit

class ExercisesCell: UICollectionViewCell {
    
    // Views
    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var favOutlet: UIButton!
    @IBOutlet weak var watermark: UIImageView!
    @IBOutlet weak var cookingLabel: UILabel!
    @IBOutlet weak var caloriesLabel: UILabel!

    var isPurchased = Bool()
}
