//
//  AboutUs.swift
//  FitnessStudio
//
//  Created by Greenfield
//  Copyright (c) 2017 Greenfield.com. All rights reserved.
//


import UIKit
import StoreKit

class Programs: UIViewController {
    
    @IBOutlet weak var getFirstProgram: UIButton!
    @IBOutlet weak var getSecondProgram: UIButton!
    @IBOutlet weak var getThirdProgram: UIButton!
    
    var programs: [SKProduct] = []
    
    //FORMATTER FOR PRICE
    static let priceFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.formatterBehavior = .behavior10_4
        formatter.numberStyle = .currency
        return formatter
    }()
    
    //SETTINGS OF BUTTON
    func buttonShreddedSettings(alpha: CGFloat, isEnabled: Bool, button: UIButton) {
        button.alpha = alpha
        button.isEnabled = isEnabled
    }
    
    //PUSH ANY VIEW CONTROLLER FROM NAVIGATION
    func pushAnyViewController(nameStoryboard: String, nameVC: String) {
        let storyboard: UIStoryboard = UIStoryboard(name: nameStoryboard, bundle: nil)
        let vc: UIViewController = storyboard.instantiateViewController(withIdentifier: nameVC) as UIViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //PRESENT ANY ALERT CONTROLLER
    func alertViewController(program: SKProduct) {
        let alert = UIAlertController(title: "Do you want to purchase the access?", message: "\(String(describing: program.localizedTitle)) - \(String(describing: Programs.priceFormatter.string(from: (program.price))!))", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action) in
            Products.store.buyProduct(program)
        }))
        alert.addAction(UIAlertAction(title: "Restore", style: .default, handler: { (action) in
            DispatchQueue.global(qos: .userInteractive).async {
                Products.store.restorePurchases()
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
    func reload() {
        Products.store.requestProducts{ [weak self] success, products in
            guard let sself = self else { return }
            if success {
                sself.programs = products!
                sself.hideHUD()
                sself.buttonShreddedSettings(alpha: 1, isEnabled: true, button: sself.getFirstProgram)
                sself.buttonShreddedSettings(alpha: 1, isEnabled: true, button: sself.getSecondProgram)
                sself.buttonShreddedSettings(alpha: 1, isEnabled: true, button: sself.getThirdProgram)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showHUD("Programs...")
        buttonShreddedSettings(alpha: 0.4, isEnabled: false, button: self.getFirstProgram)
        buttonShreddedSettings(alpha: 0.4, isEnabled: false, button: self.getSecondProgram)
        buttonShreddedSettings(alpha: 0.4, isEnabled: false, button: self.getThirdProgram)
        DispatchQueue.global(qos: .userInteractive).async {
            self.reload()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buyFirstPrograms(_ sender: UIButton) {
        if Products.store.isProgramPurchased(programs[Constants.firstPurchase].productIdentifier) {
            
            pushAnyViewController(nameStoryboard: "Main", nameVC: "GET SHREDDED")
            
        } else if IAPHelper.canMakePayments() {
            
            alertViewController(program: self.programs[Constants.firstPurchase])
            
        } else {
            print("Not available")
        }
    }
    
    @IBAction func buySecondPrograms(_ sender: UIButton) {
        if Products.store.isProgramPurchased(programs[Constants.secondPurchase].productIdentifier) {
            
            pushAnyViewController(nameStoryboard: "Main", nameVC: "SECOND PURCHASE")
            
        } else if IAPHelper.canMakePayments() {
            
            alertViewController(program: self.programs[Constants.secondPurchase])
            
        } else {
            print("Not available")
        }
    }
    
    @IBAction func buyThirdPrograms(_ sender: UIButton) {
        if Products.store.isProgramPurchased(programs[Constants.thirdPurchase].productIdentifier) {
            
            pushAnyViewController(nameStoryboard: "Main", nameVC: "THIRD PURCHASE")
            
        } else if IAPHelper.canMakePayments() {
            
            alertViewController(program: self.programs[Constants.thirdPurchase])
            
        } else {
            print("Not available")
        }
    }
    
    @IBAction func backToMainView(_ sender: UIBarButtonItem) {
        categoryStr = ""
        self.tabBarController?.navigationController?.popViewController(animated: true)
        self.tabBarController?.navigationController?.isNavigationBarHidden = false
    }
}

//MARK: - Constants
extension Programs {
    private enum Constants {
        static let firstPurchase = 0
        static let secondPurchase = 1
        static let thirdPurchase = 2
    }
}
